const express = require("express");
const http = require("http");
const fs = require("fs").promises;
const path = require("path");
const uuid = require("uuid");
const app = express();

const port = 5000;

app.get("/html", (req, res) => {
  res.setHeader("content-type", "text/html");
  fs.readFile(path.join(__dirname, "index.html"), "utf-8")
  .then((data)=>{
    return res.send(data);
  })
.catch((err)=>{
  return res.send(err)
})
});

app.get("/json", (req, res) => {
  res.setHeader("content-type", "application/json");
  fs.readFile(path.join(__dirname, "index.json")).then((data)=>{
    return res.send(data);
  })
.catch((err)=>{
  return res.send(err)
});
});

app.get("/uuid", (req, res) => {
  res.setHeader("content-type", "application/json");
  return res.send(`{"uuid" : ${JSON.stringify(uuid.v4())}}`);
});

app.get("/status/*", (req, res) => {
  res.setHeader("content-type", "application/json");
  var url = req.url.split("/");
  let statuscode = parseInt(url[url.length - 1]);
  if (typeof statuscode === "number") {
    return res.send(`${statuscode},${http.STATUS_CODES[statuscode]}`);
  }
});

app.get("/delay/*", (req, res) => {
  res.setHeader("content-type", "application/json");
  var url = req.url.split("/");
  let code = 200;
  var delaytime = parseInt(url[url.length - 1]);
  if (typeof delaytime === "number") {
    setTimeout(() => {
      return res.status(200).json({ message: "ok" });
    }, delaytime * 1000);
  }
});

app.listen(port, () => {
  console.log(`hey start running on ${port}`);
});
